﻿using DataAccess.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class Favorites : TransactionController
    {
        public Favorites(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public IList<FavoriteDB> GetAll()
        {
            IList<FavoriteDB> result = null;
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                ICriteria criteria = session.CreateCriteria<FavoriteDB>();
                result = criteria.List<FavoriteDB>();
            });
            return result;
        }

        public FavoriteDB Add(FavoriteDB text)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(text);
            });
            return text;
        }
    }
}
