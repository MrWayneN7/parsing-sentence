﻿using DataAccess.Entities;
using NHibernate;
using System.Collections.Generic;

namespace DataAccess
{
    public class Texts:TransactionController
    {
        public Texts(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public IList<TextDB> GetAll()
        {
            IList<TextDB> result = null;
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                ICriteria criteria = session.CreateCriteria<TextDB>();
                result = criteria.List<TextDB>();
            });
            return result;
        }

        public TextDB Add(TextDB text)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(text);
            });
            return text;
        }
    }
}
