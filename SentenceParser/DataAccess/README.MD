﻿# Использование
1) Подключить к проекту зависимость DataAccess.dll или сам проект в качестве зависимости
2) Через NuGet добавить следующие зависимости: 
1. NHibernate
2. NHibernate.Mapping.Attributes
3. Npgsql
4. Остальное должно само подтянуться

В коде:
~~~~
 DataBase db = new DataBase();
 Console.WriteLine("Connection to data base...");
 db.Connect("server=127.0.0.1;port=5432;database=SentenceParser;user id=sp;password=sp;timeout=1000;");//Объяснять не буду, и так ясно
 UserDB user = db.Users.Add(new UserDB("login", "pswd_hash", "email", System.DateTime.Now));//Например добавить пользователя
 db.Texts.Add(new TextDB(user,System.DateTime.Now,"textPath"));//Добавить текст
 //и тд.
 //Перед завершением
 db.Close()//Освобождаем ресурсы;
~~~~