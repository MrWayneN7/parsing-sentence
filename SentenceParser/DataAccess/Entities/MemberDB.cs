﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    [Class]
    public class MemberDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }

        [Property]
        public virtual string Name { get; set; }

        public MemberDB() { }
        public MemberDB(String name)
        {
            Name = name;
        }
    }
}
