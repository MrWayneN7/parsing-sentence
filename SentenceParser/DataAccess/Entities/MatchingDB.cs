﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    [Class]
    public class MatchingDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }

        [ManyToOne(Name = "Attribute", Column = "AttributeId", ClassType = typeof(AttributeDB), Cascade = "save-update")]
        public virtual AttributeDB Attribute { set; get; }
        [ManyToOne(Name = "Member", Column = "MemberId", ClassType = typeof(MemberDB), Cascade = "save-update")]
        public virtual MemberDB Member { set; get; }

        public MatchingDB() { }
        public MatchingDB(AttributeDB attribute, MemberDB member)
        {
            Attribute = attribute;
            Member = member;
        }
    }
}
