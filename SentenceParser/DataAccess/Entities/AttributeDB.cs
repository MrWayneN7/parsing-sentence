﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    [Class]
    public class AttributeDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }

        [Property]
        public virtual string Name { get; set; }

        public AttributeDB() { }
        public AttributeDB(String name)
        {
            Name = name;
        }
    }
}
