﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    [Class]
    public class UserDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }
        [Property]
        public virtual string Password { get; set; }
        [Property]
        public virtual string Login { get; set; }
        [Property]
        public virtual DateTime Reg_date { get; set; }
        [Property]
        public virtual string Email { get; set; }

       

        public UserDB()
        {

        }

        public UserDB(String userLogin,String userPassword, String userEmail, DateTime userReg_date)
        {
            Login = userLogin;
            Password = userPassword;
            Email = userEmail;
            Reg_date = userReg_date;
        }
    }
}
