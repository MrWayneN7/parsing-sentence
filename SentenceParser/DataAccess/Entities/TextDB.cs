﻿using NHibernate.Mapping.Attributes;
using System;

namespace DataAccess.Entities
{
    [Class]
    public class TextDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }

        [ManyToOne(Name = "User", Column = "UserId", ClassType = typeof(UserDB), Cascade = "save-update")]
        public virtual UserDB User { set; get; }

        [Property]
        public virtual DateTime Process_datetime { get; set; }

        [Property]
        public virtual string Processed_text_path { get; set; }

        public TextDB()
        {

        }

        public TextDB(UserDB user,DateTime process_datetime,String processed_text_path)
        {
            User = user;
            Processed_text_path = processed_text_path;
            Process_datetime = process_datetime;
        }
    }
}
