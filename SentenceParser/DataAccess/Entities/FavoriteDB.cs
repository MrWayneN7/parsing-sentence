﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    [Class]
    public class FavoriteDB
    {
        [Id(0, Name = "ID")]
        [Generator(1, Class = "native")]
        public virtual long ID { get; set; }

        [ManyToOne(Name = "Text", Column = "TextId", ClassType = typeof(TextDB), Cascade = "save-update")]
        public virtual TextDB Text { set; get; }
        [ManyToOne(Name = "User", Column = "UserId", ClassType = typeof(UserDB), Cascade = "save-update")]
        public virtual UserDB User { set; get; }

        public FavoriteDB() { }
        public FavoriteDB(TextDB text, UserDB user)
        {
            Text = text;
            User = user;
        }
    }
}
