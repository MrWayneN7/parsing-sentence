﻿using DataAccess.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class Members : TransactionController
    {
        public Members(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public IList<MemberDB> GetAll()
        {
            IList<MemberDB> result = null;
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                ICriteria criteria = session.CreateCriteria<MemberDB>();
                result = criteria.List<MemberDB>();
            });
            return result;
        }

        public MemberDB Add(MemberDB text)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(text);
            });
            return text;
        }
    }
}
