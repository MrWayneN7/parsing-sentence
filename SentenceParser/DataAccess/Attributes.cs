﻿using DataAccess.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class Attributes : TransactionController
    {
        public Attributes(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public IList<AttributeDB> GetAll()
        {
            IList<AttributeDB> result = null;
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                ICriteria criteria = session.CreateCriteria<AttributeDB>();
                result = criteria.List<AttributeDB>();
            });
            return result;
        }

        public AttributeDB Add(AttributeDB text)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(text);
            });
            return text;
        }
    }
}
