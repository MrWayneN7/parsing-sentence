﻿using DataAccess.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class Users : TransactionController
    {
        public Users(ISessionFactory sessionFactory):base(sessionFactory)
        {
        }

        public IList<UserDB> GetAll()
        {
            IList<UserDB> result = null;
            BeginTransaction(delegate (ITransaction tx,ISession session) {
                ICriteria criteria = session.CreateCriteria<UserDB>();
                result = criteria.List<UserDB>();
            });
            return result;
        }

        public UserDB Add(UserDB user)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(user);
            });
            return user;
        }
            
    }
}
