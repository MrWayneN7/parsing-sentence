﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Reflection;

namespace DataAccess
{
    public class DataBase
    {
        private Configuration configuration = new Configuration();
        private ISessionFactory sessionFactory = null;

        public Users Users { get; private set; } = null;
        public Texts Texts { get; private set; } = null;
        public Favorites Favorites { get; private set; } = null;
        public Members Members { get; private set; } = null;
        public Attributes Attributes { get; private set; } = null;
        public Matching Matching { get; private set; } = null;

        public DataBase()
        {
        }

        public void Connect(String connectionString)
        {
            configuration.DataBaseIntegration(x =>
            {
                x.ConnectionString = connectionString; 
                x.Driver<NpgsqlDriver>();
                x.Dialect<PostgreSQLDialect>();
                       
            });
            NHibernate.Mapping.Attributes.HbmSerializer.Default.Validate = true;
            configuration.AddInputStream(NHibernate.Mapping.Attributes.HbmSerializer.Default.Serialize(System.Reflection.Assembly.GetExecutingAssembly()));
            configuration.AddAssembly(Assembly.LoadFrom("DataAccess.dll"));
            new SchemaUpdate(configuration).Execute(true, true);
            sessionFactory = configuration.BuildSessionFactory();

            Users = new Users(sessionFactory);
            Texts = new Texts(sessionFactory);
            Favorites = new Favorites(sessionFactory);
            Members = new Members(sessionFactory);
            Attributes = new Attributes(sessionFactory);
            Matching = new Matching(sessionFactory);
        }

        public void Close()
        {
            if (sessionFactory != null)
                sessionFactory.Close();
        }

        
    }
}
