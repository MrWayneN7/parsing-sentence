﻿using NHibernate;

namespace DataAccess
{
    public abstract class TransactionController
    {
        protected delegate void Expression(ITransaction trx,ISession session);
        protected  ISessionFactory sessionFactory = null;

        public TransactionController(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        protected void BeginTransaction(Expression expression)
        {
            
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    expression(tx,session);
                    tx.Commit();
                }
            }
        }
    }
}
