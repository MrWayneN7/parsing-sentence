﻿using DataAccess.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class Matching : TransactionController
    {
        public Matching (ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public IList<MatchingDB> GetAll()
        {
            IList<MatchingDB> result = null;
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                ICriteria criteria = session.CreateCriteria<MatchingDB>();
                result = criteria.List<MatchingDB>();
            });
            return result;
        }

        public MatchingDB Add(MatchingDB text)
        {
            BeginTransaction(delegate (ITransaction tx, ISession session) {
                session.Save(text);
            });
            return text;
        }
    }
}
