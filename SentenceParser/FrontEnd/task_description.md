Необходимо создать приложение, разбивающее текст на члены предложения. 
Основные требования:
1. текст должен быть на русском языке;
2. на вход приложения текст поступает в файле txt или doc (docx);
3. точность распознавания: не менее 75%;
4. архитектура приложения: клиент-сервер, концепция MVC .
Также необходимо придумать дополнительные функции (3 функции).
 
Use-cases приложения: 
- получение разобранного текста на члены предложения 
- регистрация 
- авторизация 
- получение истории последних разборов 
- добавление разобранных текстов в избранное с последующим доступом к "избранному" 
- скачивание разобранных текстов 
- получение справочных сведений о членах предложения 
- получение статистики по частоте встречаемости различных членов предложения в тексте 
- определение типа предложений