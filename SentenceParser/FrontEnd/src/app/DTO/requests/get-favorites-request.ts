export class GetFavoritesRequest {
    login: string;

    constructor(login: string) {
        this.login = login;
    }
}