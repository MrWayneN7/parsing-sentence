export class SetFavoritesRequest {
    login: string;
    text: string;

    constructor(login: string, text: string) {
        this.login = login;
        this.text = text;
    }
}