export class TextParsingRequest {
    text: string;

    constructor(text: string) {
        this.text = text;
    }
}