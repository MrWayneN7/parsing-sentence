import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TextParsingRequest } from './DTO/requests/text-parsing-request';
import { TextParsingResponse } from './DTO/responses/text-parsing-response';
import { LoginRequest } from './DTO/requests/login-request';
import { SetFavoritesRequest } from './DTO/requests/set-favorites-request';
import { GetFavoritesRequest } from './DTO/requests/get-favorites-request';
import { LoginResponse } from './DTO/responses/login-response';

@Injectable()
export class AppService {

    constructor(private http: HttpClient) { }

    public textParsing(request: TextParsingRequest): Observable<TextParsingResponse> {
        return this.postData("Text/TextParsing", request);
    }

    public login(request: LoginRequest): Observable<LoginResponse> {
        return this.postData("Text/Login", request);
    }

    public getFavorites(request: GetFavoritesRequest): Observable<TextParsingResponse> {
        return this.postData("Text/GetFavorites", request);
    }

    public setFavorites(request: SetFavoritesRequest): Observable<TextParsingResponse> {
        return this.postData("Text/SetFavorites", request);
    }

    public register(request: TextParsingRequest): Observable<TextParsingResponse> {
        return this.postData("Text/TextParsingRequest", request);
    }

    public textParsingFromFile(request: any): Observable<TextParsingResponse> {
        return this.postData("Text/TextParsingFromFile", request);
    }

    private postData(url: string, request: any): Observable<any> {
        url = "http://localhost:49858/api/" + url;
        return this.http.post(url, request);
    }
}