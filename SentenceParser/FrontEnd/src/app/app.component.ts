import { Component } from '@angular/core';
import { AppService } from './app.service';
import { TextParsingRequest } from './DTO/requests/text-parsing-request';
import { LoginRequest } from './DTO/requests/login-request';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public parsingText: string;
  public userLogin: string;
  public isShowLoginForm: boolean;

  public login: string;
  public password: string;
  public conPassword: string;

  constructor(private appService: AppService) {
    this.userLogin = localStorage.getItem('userLogin')
  }

  private sendToParseText() {
    this.appService.textParsing(new TextParsingRequest(this.parsingText)).subscribe(x => {
      var a = 0;
    });
  }

  private toggleLoginForm() {
    this.isShowLoginForm = !this.isShowLoginForm;
  }

  private loginIn() {
    this.appService.login(new LoginRequest(this.login, this.password)).subscribe(x => {
      if (x.isSuccessful) {
        localStorage.setItem('userLogin', this.login);
        this.userLogin = this.login;
        this.isShowLoginForm = false;
      }
    });
  }
}
