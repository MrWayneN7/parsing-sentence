﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Entities;

namespace DBTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            Console.WriteLine("Connection to data base...");
            db.Connect("server=127.0.0.1;port=5432;database=SentenceParser;user id=sp;password=sp;timeout=1000;");
            UserDB user = db.Users.Add(new UserDB("login", "pswd_hash", "email", System.DateTime.Now));
            db.Texts.Add(new TextDB(user,System.DateTime.Now,"textPath"));
            Console.WriteLine("Success");
            db.Close();
        }
    }
}
