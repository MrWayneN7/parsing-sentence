using BackEnd.Models.Requests;
using BackEnd.Models.Responses;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd.Controllers
{
  [Route("api/Text")]
  [ApiController]
  public class TextController : ControllerBase
  {
    [HttpPost("TextParsing")]
    public TextParsingResponse TextParsing([FromBody] TextParsingRequest request)
    {
			return new TextParsingResponse(request.Text);
    }

    [HttpPost("Login")]
    public LoginResponse Login([FromBody] LoginRequest request)
    {
      return new LoginResponse(request.Login);
    }

    [HttpPost("GetFavorites")]
    public GetFavoritesResponse GetFavorites([FromBody] GetFavoritesRequest request)
    {
			return new GetFavoritesResponse(request.Login);
    }

    [HttpPost("SetFavorites")]
    public SetFavoritesResponse SetFavorites([FromBody] SetFavoritesRequest request)
		{
			return new SetFavoritesResponse(request.Login, request.Text);
		}

    [HttpPost("Register")]
    public RegisterResponse Register([FromBody] RegisterRequest request)
    {
			return new RegisterResponse(request.Login, request.Password);
    }
  }
}
