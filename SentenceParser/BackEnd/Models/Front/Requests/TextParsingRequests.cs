namespace BackEnd.Models.Requests
{
  public class TextParsingRequest
  {
    public string Text { get; set; }
  }
}
