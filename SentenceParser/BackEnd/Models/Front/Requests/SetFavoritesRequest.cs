namespace BackEnd.Models.Requests
{
  public class SetFavoritesRequest
  {
    public string Login { get; set; }

    public string Text { get; set; }
  }
}
