namespace BackEnd.Models.Requests
{
  public class GetFavoritesRequest
  {
    public string Login { get; set; }
  }
}
