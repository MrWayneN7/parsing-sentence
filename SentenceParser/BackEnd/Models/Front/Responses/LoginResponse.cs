namespace BackEnd.Models.Responses
{
  public class LoginResponse
  {
    private bool IsSuccess { get; set; }

    public LoginResponse(string login)
    {
      if (login != "Piska")
        IsSuccess = false;
      else
        IsSuccess = true;
    }
  }
}
