using System.Collections.Generic;

namespace BackEnd.Models.Responses
{
  public class GetFavoritesResponse
  {
    public string Login { get; set; }

    public GetFavoritesResponse(string login)
    {
      Login = login;
    }
  }
}
