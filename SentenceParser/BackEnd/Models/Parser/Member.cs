using BackEnd.Env;
using Newtonsoft.Json;

namespace BackEnd.Model
{
  public class Member
  {
    [JsonProperty("word")]
    public string Word { get; set; }

    [JsonProperty("pos_tag")]
    public string PosTag { get; set; }

    [JsonProperty("label")]
    public string Label { get; set; }

    public void Tramsform()
    {
      PosTag = Dicts.PosTags[PosTag.ToUpper()];
      Label = Dicts.PartsOfSentence[Label.ToUpper()];
    }
  }
}
