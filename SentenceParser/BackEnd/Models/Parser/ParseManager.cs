using System;
using System.Collections.Generic;
using BackEnd.Model;
using RestSharp;

namespace BackEnd.Models
{
  public class ParsedSentense
  {
    public string Input { get; set; }
    public List<Member> Output { get; set; }
  }

  public static class ParseManager
  {
    private static RestClient client = new RestClient("http://192.168.99.100:9000/api/v1/");

    public static void SelectLang(string lang)
    {
      var request = new RestRequest("use/" + lang, Method.GET);

      var asyncHandle = client.ExecuteAsync<List<ParsedSentense>>(request, response => {
        Console.WriteLine(response.Data);
      });
    }

    public static void ParseText(List<string> sentence)
		{
      var request = new RestRequest("query", Method.POST);
      request.AddJsonBody(new { strings = sentence, tree = false });

      var asyncHandle = client.ExecuteAsync<List<ParsedSentense>>(request, response => {
        Console.WriteLine(response.Data);
      });
    }
  }
}
