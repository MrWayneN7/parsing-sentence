using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BackEnd.Env
{
  class Config
  {
    public static string CurrentDir { get; private set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\");
  }
}
