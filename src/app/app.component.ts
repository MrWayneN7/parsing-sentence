import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Parcing sentence';

  // переменная для хранения результата разбора
  public resultData: string = null;
  
  // переменная для хранения введенного текста
  public textData: string = null;

  // функция получения информации о приложении
  public toggleButtonInfo(): void {}

  // функция запроса данных на обработку
  public toggleSubmitButton(): void {
    // let textData = document.getElementById('textarea1').value;
    // this.textData = textData;
  }
  
  // функция очистки полей ввода
  public toggleClearButton(): void {
    this.textData = null;
    this.resultData = null;
  }
}
